CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;
use usermanagement;
CREATE TABLE user (
id SERIAL PRIMARY KEY UNIQUE NOT NULL AUTO_INCREMENT,
login_id  varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date DATE NOT NULL,
password  varchar(255) DEFAULT NULL,
create_date  DATETIME NOT NULL,
update_date  DATETIME NOT NULL,);

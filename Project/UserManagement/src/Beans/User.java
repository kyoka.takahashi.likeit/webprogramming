package Beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable{

	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private String createDate;
	private String updateDate;



	public User(String loginIdDate, String nameDate) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.loginId = loginIdDate;
		this.name = nameDate;
	}
	public User(int id, String loginId, String name, Date birthDate, String password, String createDate,
			String updateDate) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}
	public User(String loginIdDate, String nameDate, Date birthdateDate, String createDate2, String updateDate2) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.loginId = loginIdDate;
		this.name = nameDate;
		this.birthDate = birthdateDate;
		this.createDate = createDate2;
		this.updateDate = updateDate2;
	}
	public User(String password, String nameDate, Date birthdateDate, String loginIdDate) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.password = password;
		this.name = nameDate;
		this.birthDate = birthdateDate;
		this.loginId = loginIdDate;

	}

	public User(String loginId, String password, String name,Date birthdate) {
		// TODO 自動生成されたコンストラクター・スタブ
		this.loginId = loginId;
		this.password = password;
		this.name = name;
		this.birthDate = birthdate;

	}

	public User(String loginId) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	public User() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


}

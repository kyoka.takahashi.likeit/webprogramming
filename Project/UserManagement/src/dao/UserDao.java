package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import Beans.User;

public class UserDao {

	public User findByLoginInfo(String loginId, String password) {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			return new User(loginIdDate, nameDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User findById(String id) {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String loginIdDate = rs.getString("login_id");
			String nameDate = rs.getString("name");
			Date birthdateDate = rs.getDate("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(loginIdDate, nameDate, birthdateDate, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public List<User> findAll() {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id != 1";

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}



	public List<User> Search (String name, String loginId,String startDate,String endDate) {
		Connection con = null;
		List<User> userList = new ArrayList<User>();

		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id != 1";

			if(!name.equals("")) {
				sql += " AND name LIKE '%"+name+"%'";
			}

			if(!loginId.equals("")) {
				sql += " AND login_Id = '"+ loginId +"'" ;
			}

			if(!startDate.equals("")) {
				sql += " AND birth_date >= '"+ startDate+"'";
			}

			if(!endDate.equals("")) {
				sql += " AND birth_date <='"+ endDate+ "'";
			}

			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginIdDate = rs.getString("login_id");
				String username = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginIdDate, username, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;

	}



	public User create (String loginId, String password,String Cpassword, String name, String birthdate) {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "INSERT INTO user (login_id, password,name,birth_date,create_date,update_date) VALUES (?,?,?,?,NOW(),NOW()) ";

			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, loginId);
			stmt.setString(2, password);
			stmt.setString(3, name);
			stmt.setString(4, birthdate);
			stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return null;
	}


	public User Id (String loginId) {
		Connection con = null;

		try {
			con = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();


			if (!rs.next()) {
				return null;
			}
			String loginIdDate = rs.getString("login_id");
			return new User (loginIdDate);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}



	public User Update(String password,String Cpassword, String name, String birthdate,String loginId) {
		Connection con = null;
		User user = new User();
		try {
			con = DBManager.getConnection();

			String sql = "UPDATE user SET password = ?,name = ?,birth_date = ? ,update_date = NOW() WHERE login_id = ? ";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, password);
			pStmt.setString(2, name);
			pStmt.setString(3, birthdate);
			pStmt.setString(4, loginId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return user;
	}

	public User Delete(String loginId) {
		Connection con = null;
		User user = new User();
		try {
			con = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE login_Id = ?";

			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return user;
	}


	public String MD5 (String password)   {

		Charset charset = StandardCharsets.UTF_8;
		String algorithm = "MD5";
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(password.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

		return result ;

	}
}


package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.User;
import dao.UserDao;

/**
 * Servlet implementation class CreateServlet
 */
@WebServlet("/CreateServlet")
public class CreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings({ "unused" })
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String Cpassword = request.getParameter("Cpassword");
		String username = request.getParameter("name");
		String birthdate = request.getParameter("birthdate");


		//既存ログインID
		UserDao dao = new UserDao();
		User Id = dao.Id(loginId);
		if(Id != null) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			request.setAttribute("loginId",loginId);
			request.setAttribute("name",username);
			request.setAttribute("birthDate",birthdate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request, response);
			return;
		}


		// パスワード
		if(! password.equals (Cpassword)) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			request.setAttribute("loginId",loginId);
			request.setAttribute("name",username);
			request.setAttribute("birthDate",birthdate);
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
			dispatcher.forward(request, response);
			return;

		}

		// 未入力
		if (loginId==null || password==null || Cpassword==null || username==null || birthdate==null) {
			 request.setAttribute("errMsg", "入力された内容は正しくありません。");
			 request.setAttribute("loginId",loginId);
			 request.setAttribute("name",username);
			 request.setAttribute("birthDate",birthdate);
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/create.jsp");
				dispatcher.forward(request, response);
				return;

		}else {
			UserDao userDao = new UserDao();
			String md = userDao.MD5(password);
			password = md;
			User user = userDao.create(loginId,password,Cpassword,username,birthdate);

			response.sendRedirect("UserListServlet");

		 }

	}

}

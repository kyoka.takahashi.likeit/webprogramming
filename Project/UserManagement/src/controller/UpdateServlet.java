package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Beans.User;
import dao.UserDao;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		System.out.println(id);

		UserDao userDao = new UserDao();
		User user = userDao.findById(id);
		request.setAttribute("loginId",user.getLoginId());
		request.setAttribute("name",user.getName());
		request.setAttribute("birthDate",user.getBirthDate());

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 request.setCharacterEncoding("UTF-8");

		 String loginId = request.getParameter("loginId");
		 String password = request.getParameter("password");
		 String Cpassword = request.getParameter("Cpassword");
		 String name = request.getParameter("name");
		 String birthdate = request.getParameter("birthdate");

		 //パスワード
		 if(! password.equals (Cpassword)) {
				request.setAttribute("errMsg", "入力された内容は正しくありません。");
				request.setAttribute("loginId",loginId);
				request.setAttribute("name",name);
				request.setAttribute("birthDate",birthdate);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
				dispatcher.forward(request, response);
				return;
		 }

		 if(password == null && Cpassword == null) {
			UserDao userDao = new UserDao();
			User user = userDao.Update(password,Cpassword,name,birthdate,loginId);
			response.sendRedirect("UserListServlet");
		 }

		 //未入力
		if (password == null || Cpassword == null || name == null || birthdate == null ) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			request.setAttribute("loginId",loginId);
			request.setAttribute("name",name);
			request.setAttribute("birthDate",birthdate);

			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
				dispatcher.forward(request, response);
				return;

		}else {
			UserDao userDao = new UserDao();
			String md = userDao.MD5(password);
			password = md;
			User user = userDao.Update(password,Cpassword,name,birthdate,loginId);

			response.sendRedirect("UserListServlet");

		 }


	}

}

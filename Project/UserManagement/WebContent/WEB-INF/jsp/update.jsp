<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<div class="bg-warning">
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Control Sidebar Toggle Button -->
				<li><a class="nav-link">${userInfo.name} さん</a></li>
				<li><a class="nav-link" href="LogoutServlet"> <i
						class="fa fa-fw fa-sign-out"></i>ログアウト
				</a></li>
			</ul>
		</div>
	</div>

	<br>
	<div class="container">
		<div class="row justify-content-center">
				<div class="col-auto">
				<h1>ユーザ情報更新</h1>
			</div>
			<br>
		</div>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>


		<form class="form-signin" action="UpdateServlet" method="post">
			<div class="row justify-content-center">
				<div class="col-auto">

					<div class="form-group row">
						<div class="form-inline col-12">
							<label for="id" class="col-5 col-form-label">ログインID</label>
								<div class="col-5 offset-2">${loginId}</div>
									<input type="hidden" name="loginId" class="form-control" value=${loginId}>
						</div>
					</div>

					<div class="form-group row ">
						<div class="form-inline col-12">
							<label for="id" class="col-5 col-form-label">パスワード</label>
							 <div class="col-5 offset-2">
							 <input type="text" name="password" class="form-control">
							</div>
						</div>
					</div>

					<div class="form-group row ">
						<div class="form-inline col-12">
							<label for="id" class="col-5 col-form-label">パスワード(確認)</label>
								<div class="col-5 offset-2">
									<input	type="text" name="Cpassword" class="form-control">
								</div>
						</div>
					</div>

					<div class="form-group row ">
						<div class="form-inline col-12">
							<label for="id" class="col-5 col-form-label">ユーザ名</label>
								<div class="col-5 offset-2">
									<input type="text" name="name" class="form-control" value=${name}>
								</div>
						</div>
					</div>

					<div class="form-group row ">
						<div class="form-inline col-12">
							<label for="id" class="col-5 col-form-label">生年月日</label>
								<div class="col-5 offset-2">
									 <input	type="text" name="birthdate" class="form-control" value=${birthDate}>
								</div>
						</div>
					</div>
				</div>
			</div>



			<div class="form-group">
				<div class="row justify-content-center">
					<div class="col-auto">
						<br>
						<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">登録</button>
						<br>
					</div>
				</div>
			</div>
		</form>
	</div>
	<p class="form-group">
		<a href="UserListServlet">戻る</a>
	</p>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html >
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>


<header>
	<nav class="navbar navbar-inverse">
		<div class="container">
			<div class="navbar-header">
				<p class="navbar-brand">ユーザ管理システム</p>
			</div>

		</div>
	</nav>
</header>


<body>
	<div class="container">
		<br>
		<div class="row justify-content-center">
			<div class="col-auto">
				<h1>ログイン画面</h1>
			</div>
		</div>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>


		<div class="row justify-content-center">
			<div class="col-auto">
					<br>
					<form class="form-signin" action="LoginServlet" method="post">
						<input type="text" name="loginId" id="inputLoginId"
							class="form-control" placeholder="ログインID" required autofocus>
						<br> <input type="password" name="password" id="inputPassword"
							 class="form-control" placeholder="パスワード" required>
						<br><button class="btn btn-lg btn-primary btn-block btn-signin"
							type="submit">ログイン</button>
					</form>
				</div>
			</div>
		</div>

</body>
</html>